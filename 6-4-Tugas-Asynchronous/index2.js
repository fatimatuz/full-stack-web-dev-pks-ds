var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// JAWABAN SOAL 2: Lanjutkan code untuk menjalankan function readBooksPromise
readBooksPromise(10000, books[0]).then(function(sisaW1){
    readBooksPromise(sisaW1, books[1]).then(function(sisaW2){
        readBooksPromise(sisaW2, books[2]).then(function(sisaW3){
            readBooksPromise(sisaW3, books[3]).then(function(sisaW4){
                readBooksPromise(sisaW4, books[3]).then(function(sisaW5){ //-->untuk cek sisa waktu habis
                }).catch(error => console.log(error))
            }).catch(error => console.log(error))
        }).catch(error => console.log(error))
    }).catch(error => console.log(error))
}).catch(error => console.log(error))