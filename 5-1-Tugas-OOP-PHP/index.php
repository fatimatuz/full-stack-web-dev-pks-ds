<?php

trait Hewan {
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;
    
    public function atraksi(){        
        return $this->nama . " sedang " . $this->keahlian. "<br><br>";
    }
    public function getInfoHewan(){
        return $this->nama . " adalah jenis hewan " . $this->jenis . " yang berkaki " . $this->jumlahKaki . " dengan keahlian berupa " . $this->keahlian . 
        "<br> Status Petarung: <br> AttackPower = " . $this->attackPower . "<br> DefencePower = " . $this->defencePower . "<br> Jumlah darah = ". $this->darah ."<br><br>";
    }
}

trait Fight {
    public $attackPower;
    public $defencePower;
       
    public function serang($korban, $dpkorban, $darahkorban){
        $this->korban = $korban;
        $this->dpkorban = $dpkorban;
        $this->darahkorban = $darahkorban;
        $this->diserang($this->nama, $this->attackPower);
    }

    public function diserang($penyerang, $appenyerang){
        $this->penyerang = $penyerang;
        $this->appenyerang = $appenyerang;
        
        $this->sisadarahkorban = $this->darahkorban - ($this->appenyerang/$this->dpkorban);
        
        echo $this->penyerang . " sedang menyerang " .$this->korban ."<br><br>". $this->korban . " sedang diserang " . $this->penyerang .
        "<br> Sisa Darah " . $this->korban . " adalah = ". $this->sisadarahkorban . "<br><br>";
    }

        
}

class Elang{
    use Hewan, Fight;
    public function __construct($nama){
        $this->nama = $nama;
        $this->jenis = "Elang";
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
    }
}

class Harimau{
    use Hewan, Fight;
    public function __construct($nama){
        $this->nama = $nama;
        $this->jenis = "Harimau";
        $this->jumlahKaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;
    }
}

$elang = new Elang("Eaglo");
echo $elang->getInfoHewan();
echo $elang->atraksi();

$harimau = new Harimau("Tigaro");
echo $harimau->getInfoHewan();
echo $harimau->atraksi();

echo $elang->serang("Mak Lampir", 5, 50);


?>