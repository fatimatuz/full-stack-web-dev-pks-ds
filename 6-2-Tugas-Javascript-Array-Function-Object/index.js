//SOAL 1: MENGURUTKAN ARRAY

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"]
daftarHewan.sort().forEach(function(hewan){
    console.log(hewan)
})


//SOAL 2: MENGAKSES DATA OBJECT UNTUK MEMBENTUK KALIMAT

function introduce(object){
    return '"Nama saya ' + object.name + ", umur saya " + object.age + " tahun, alamat saya di " + object.address + ", dan saya punya hobby yaitu " + object.hobby +'!"'
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan)


//SOAL 3: MENGHITUNG JUMLAH HURUF DENGAN KRITERIA VOKAL

function hitung_huruf_vokal(string){
    return string.match(/[aiueo]/gi).length
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2)


//SOAL 4: MEMBUAT FUNGSI HITUNG()

function hitung(int){
    return (int-1)*2
}

console.log( hitung(0) )
console.log( hitung(1) )
console.log( hitung(2) )
console.log( hitung(3) )
console.log( hitung(5) )




