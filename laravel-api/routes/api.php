<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Tugas Pekan 5 Hari-5: Membuat Web API dan Authentication Menggunakan JWT 2
Route::middleware('auth:api')->group(function(){
    Route::apiResource('/post', 'PostController');
    Route::apiResource('/comment', 'CommentController');
    Route::post('/auth/change-password', 'Auth\ChangePasswordController')->name('auth.change_password');
    
    Route::get('/tes', function(){
        return 'Yeah, kamu berhasil login';
    });
});

Route::apiResource('/role', 'RoleController');

Route::group([
    'prefix' => 'auth',
    'namespace' => 'Auth'
], function() {
    Route::post('register', 'RegisterController')->name('auth.register');
    Route::post('regenerate-otp-code', 'RegenerateOtpCodeController')->name('auth.regenerate_otp_code');
    Route::post('verification', 'VerificationController')->name('auth.verification');
    Route::post('create-password', 'CreatePasswordController')->name('auth.create_password');
    Route::post('login', 'LoginController')->name('auth.login');
    Route::post('forgot-password', 'ForgotPasswordController')->name('auth.forgot_password');
});