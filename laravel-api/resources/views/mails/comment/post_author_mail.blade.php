<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Notifikasi Komentar</title>
</head>
<body>
    Halo {{$comment->post->user->name}}, Postingan kamu yang berjudul "{{$comment->post->title}}" telah dikomentari oleh {{$comment->user->name}} dengan isi Komentar: "{{$comment->content}}".
</body>
</html>