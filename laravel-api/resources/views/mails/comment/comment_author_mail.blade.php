<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Notifikasi Komentar</title>
</head>
<body>
    Halo {{$comment->user->name}}, kamu telah memberi Komentar berisi "{{$comment->content}}" pada Postingan milik {{$comment->post->user->name}} yang berjudul "{{$comment->post->title}}".
</body>
</html>