<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['id', 'name'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model){
            if (empty($model->id)){
                $model->id = Str::uuid();
            }
        });
    }
    
    public function user()
    {
        return $this->hasMany('App\User');
    }
}
//Create Database dengan UUID berhasil dengan Laravel Tinker checked Ok