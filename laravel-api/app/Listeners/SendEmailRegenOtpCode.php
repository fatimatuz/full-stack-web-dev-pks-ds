<?php

namespace App\Listeners;

use App\Events\RegenOtpCodeEvent;
use App\Mail\RegenOtpCodeMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmailRegenOtpCode implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegenOtpCodeEvent  $event
     * @return void
     */
    public function handle(RegenOtpCodeEvent $event)
    {
        Mail::to($event->otp_code->user->email)->send(new RegenOtpCodeMail($event->otp_code));
    }
}
