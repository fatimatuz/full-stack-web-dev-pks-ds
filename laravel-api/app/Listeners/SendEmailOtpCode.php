<?php

namespace App\Listeners;

use App\Events\OtpCodeEvent;
use App\Mail\OtpCodeMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmailOtpCode implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OtpCodeEvent  $event
     * @return void
     */
    public function handle(OtpCodeEvent $event)
    {
        Mail::to($event->otp_code->user->email)->send(new OtpCodeMail($event->otp_code));
    }
}
