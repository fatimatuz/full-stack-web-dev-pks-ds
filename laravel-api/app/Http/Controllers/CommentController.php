<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use App\Events\CommentStoredEvent;
/*use App\Mail\PostAuthorMail;
use App\Mail\CommentAuthorMail;
use Illuminate\Support\Facades\Mail;*/
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    /**
     * index
     *
     * @return void
     */
    //public function index($post_id)-> di kunci jawaban
    public function index()
    {
        //get data from table comments
        $comments = Comment::latest()->get();

        /*get data comment dengan post_id tertentu
        $comments = Comment::where('post_id',$post_id)->latest()->get();*/

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List semua comment berhasil ditampilkan',
            'data'    => $comments  
        ], 200);

    }
    
     /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find comment by ID
        $comment = Comment::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail comment dengan ID: '.$id.' berhasil ditampilkan',
            'data'    => $comment 
        ], 200);

    }
    
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
            'post_id' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $comment = Comment::create([
            'content'     => $request->content,
            'post_id'   => $request->post_id
        ]);

        event(new CommentStoredEvent($comment));

        /*email untuk si penulis Postingan
        Mail::to($comment->post->user->email)->send(new PostAuthorMail($comment));

        //email untuk si penulis Komentar
        Mail::to($comment->user->email)->send(new CommentAuthorMail($comment));*/

        //success save to database
        if($comment) {

            return response()->json([
                'success' => true,
                'message' => 'comment berhasil dibuat',
                'data'    => $comment  
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'comment Failed to Save',
        ], 409);

    }
    
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $comment
     * @return void
     */
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'content'   => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find comment by ID
        $comment = Comment::findOrFail($id);

        if($comment) {

            $user = auth()->user();

            if($comment->user_id != $user->id){
                return response()->json([
                    'success' => false,
                    'message' => 'Comment yang akan diperbaharui bukan milik kamu',
                ], 403);
            }

            //update comment TANPA mengubah post_id
            $comment->update([
            'content'     => $request->content
            ]);

            return response()->json([
                'success' => true,
                'message' => 'comment dengan ID: '.$id.' berhasil diperbaharui',
                'data'    => $comment  
            ], 200);

        }

        //data comment not found
        return response()->json([
            'success' => false,
            'message' => 'comment Not Found',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find comment by ID
        $comment = Comment::findOrfail($id);

        if($comment) {

            $user = auth()->user();

            if($comment->user_id != $user->id){
                return response()->json([
                    'success' => false,
                    'message' => 'Comment yang akan dihapus bukan milik kamu',
                ], 403);
            }

            //delete comment
            $comment->delete();

            return response()->json([
                'success' => true,
                'message' => 'comment dengan ID: '.$id.' berhasil dihapus!',
            ], 200);

        }

        //data comment not found
        return response()->json([
            'success' => false,
            'message' => 'comment Not Found',
        ], 404);
    }
}
