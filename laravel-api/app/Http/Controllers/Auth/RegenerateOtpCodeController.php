<?php

namespace App\Http\Controllers\Auth;

use App\Events\RegenOtpCodeEvent;
use App\User;
use App\OtpCode;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //panggil otp_code eksisting
        $user = User::where('email', $request->email)->first();

        if ($user->otp_code){
            $user->otp_code->delete();
        }
        
        do{
            $random = mt_rand(100000,999999);
            $check = OtpCode::where('otp',$random)->first();
        } while ($check);

        $now = Carbon::now();
        $otp_code = OtpCode::create([
            'otp' => $random,
            'valid_until' => $now->addMinutes(5),
            'user_id' => $user->id
        ]);

        //langkah berikutnya kirim regenerate otp code ke email register
        event(new RegenOtpCodeEvent($otp_code));

        //success save to database
        

            return response()->json([
                'success' => true,
                'message' => 'Otp Code berhasil di-regenerate',
                'data'    => [
                    'user' => $user,
                    'otp_code' => $otp_code
                ]
            ], 201);

    }
    
}
