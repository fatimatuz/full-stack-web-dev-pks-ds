<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class ForgotPasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password_baru' => 'required|confirmed|min:6',
        ]);
      
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        
        $user = User::where('email', $request->email)->first();

        if (!$user){
            return response()->json([
                'success' => false,
                'message' => 'Email kamu tidak ditemukan'
            ], 400);
        }
        
        //kalau benar seharusnya send email request
        $user->update([
            'password' => Hash::make($request->password_baru)
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Password kamu sudah diganti',
            'data' => $user
        ]);

    }
}