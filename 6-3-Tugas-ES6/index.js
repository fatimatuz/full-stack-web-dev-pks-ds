//=== SOAL 1. ARROW FUNCTION PERSEGI PANJANG ===

const luas = (p, l) => {
    console.log("Luas: " + p*l)
}

const keliling = (p, l) => {
    console.log("Keliling: " + 2*(p+l))
}

luas (5,3)
keliling (5,3)


//=== SOAL 2 MENGUBAH CODE MENJADI ARROW FUNCTION DAN OBJECT LITERAL ES6 ===

/*const newFunction = function literal(firstName, lastName){
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
      }
    }
  }*/

//JAWABAN SOAL 2:
const newFunction = (firstName, lastName) => {
  return {
    fullName : () => {
        console.log (firstName, lastName)
      }
    }
  }

//Driver Code 
newFunction("William", "Imoh").fullName()


//=== SOAL 3 DESTRUCTURING OBJECT ===

let newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }

//JAWABAN SOAL 3:
const {firstName, lastName, address, hobby} = newObject

// Driver code
console.log(firstName, lastName, address, hobby)


//=== SOAL 4 KOMBINASI DENGAN ARRAY SPREADING ===
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

//JAWABAN SOAL 4:
let combined = [...west, ...east]

//Driver Code
console.log(combined)


//=== SOAL 5 MENYEDERHANAKAN STRING DENGAN TEMPLATE LITERAL ES6 ===
const planet = 'earth' 
const view = 'glass'

//var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 

//JAWABAN SOAL 5:
console.log(`Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`)